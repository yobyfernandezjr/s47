// alert('Hello')

// querySelector() is a method that can be used to select a specific element from our document
console.log(document.querySelector('#txt-first-name'));
// document refers to the whole page
console.log(document);

/*
	Alternative methods that we can use aside from querySelector in retrieving elements:

	Syntax:
		document.getElementById()
		document.getElementByClassName()
		document.getElementByTagName()
*/

// DOM Manipulation
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName1 = document.querySelector('#span-full-name1');
const spanFullName2 = document.querySelector('#span-full-name2');
console.log(txtFirstName);
console.log(txtLastName);
// console.log(spanFullName);

/*
	Event:
		click, hover, keypress, keyup and many others

	Event listeners:
		Allows us to let our users interact with our page. Each click or hover is an event which can trigger a function or task.

	Syntax:
		selectedElement.addEventListener('event', function);
*/

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName1.innerHTML = txtFirstName.value
});

txtLastName.addEventListener('keyup', (event) => {
	spanFullName2.innerHTML = txtLastName.value
});

/*
alternative way

txtFirstName.addEventListener('keyup', printFirstName);

function printFirstName (event) {
	spanFullName.innerHTML = txtFirstName.value
}
*/


/*
	innerHTML - is a property of an element which considers all the children of the selected element as a string.

	.value of the input text field.
*/
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});

const labelFirstName = document.querySelector('#label-first-name');

labelFirstName.addEventListener('click', (event) => {
	// txtFirstName.innerHTML = txtFirstName.value
	console.log(event);
	
	alert('You clicked the First Name Label.');
});
